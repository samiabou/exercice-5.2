package fr.cnam.foad.nfa035.badges.wallet.model;

import java.util.HashSet;
import java.util.Set;


public class DigitalWallet {



    private Set<DigitalBadge> allBadges;

    private Set<Long> deadLinesPositions;

    private Set<DigitalBadge> deletingBadges = new HashSet<DigitalBadge>();

    /**
     *
     *
     * @param allBadges
     * @param deadLinesPositions
     */
    public DigitalWallet(Set<DigitalBadge> allBadges, Set<Long> deadLinesPositions) {
        this.allBadges = allBadges;
        this.deadLinesPositions = deadLinesPositions;
    }

    /**
     * Permet dela suppression d'un badge au niveau de cet objet
     *@param badge the badge
     */
    public void badgeDeletion(DigitalBadge badge){
        deletingBadges.remove(badge);
        allBadges.remove(badge);
        deadLinesPositions.add(badge.getMetadata().getWalletPosition());
    }



    public void addDeletingBadge(DigitalBadge deletingBadge) {
        this.deletingBadges.add(deletingBadge);
    }

    public Set<DigitalBadge> getAllBadges() {
        return allBadges;
    }


    public void setAllBadges(Set<DigitalBadge> allBadges) {
        this.allBadges = allBadges;
    }


    public Set<Long> getDeadLinesPositions() {
        return deadLinesPositions;
    }


    public void setDeadLinesPositions(Set<Long> deadLinesPositions) {
        this.deadLinesPositions = deadLinesPositions;
    }


    public Set<DigitalBadge> getDeletingBadges() {
        return deletingBadges;
    }


}

