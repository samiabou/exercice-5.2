package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer;


import java.io.IOException;

/**
 * Interface définissant le comportement attendu d'un objet servant à la sérialisation d'un badge dans un wallet
 * @param <S>
 * @param <M>
 */
public interface WalletSerializer<S, M> extends ImageStreamingSerializer<S,M> {

    void rollback(S source, M media) throws IOException;
}
