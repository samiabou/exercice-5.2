# Echaffaudage d'un Client Angular sur la base d'une API Swagger

Ce petit projet montre comment générer simplement le code de base Angular pour faciliter le développement de ce qui se situe entre l'API et ... la maquette Html/css
Bref in fine on peut se concentrer uniquemenbt sur la richesse et le dynamisme des composants graphoques :)


![image](img/angular.png)


Pour arriver à ce résultat, voici la procédure, elle-même inspirée de ce tuto assez clair:
* https://medium.com/@balramchavan/generate-angular-ionic-client-code-from-openapi-swagger-rest-api-specifications-128a6899681a
* Dont le projet est sous gitHub: https://github.com/ultrasonicsoft/swagger-codegen-demo

...

## Prérequis pour faire de l'Angular

Aller sur le site de nodejs (https://nodejs.org/en/) et télécharger la dernière version LTS de nodejs, par exemple la 14.18.0: https://nodejs.org/dist/v14.18.0/node-v14.18.0-x64.msi
Installer ceci, vous aurez donc votre plateforme nodejs à jour.

## Développement par étape

### Créer un module simple

Créer un module simple que l'on nomera badges-frontend

### Créer une app automatiquement à partir du CLI angular:

Se positionner dans le module

```
$ npm install -g @angular/cli
# Oups, si ça marche pas, donc je force un peu... 
$ npm config set strict-ssl false
$ npm install -g @angular/cli

$ ng new badges-angular-frontend
```

Oups, j'ai quand même reinstallé la dernière LTS de node, puis mis à jour npm:
(Ne sert qu'à ceux qui auraient déjà une plateforme nodejs installée)

```
$ ng version
$  npm cache clean --force
$  npm cache verify
$ npm i npm@latest -g

$ ng new badges-angular-frontend
```

Cela prend un peu de temps                   ...

```
...
CREATE test/src/app/app.component.html (24585 bytes)
CREATE test/src/app/app.component.spec.ts (950 bytes)
CREATE test/src/app/app.component.ts (208 bytes)
CREATE test/src/app/app.component.css (0 bytes)
- Installing packages (npm)...

```

...Et voilà, c'est passé ;)

 - [ ] Vous pouvez tester le résultat de l'application par défaut que génère l'Angular CLI:
 - En saisissant dans votre navigateur: http://localhost:4200/ (valeurs par défaut)
 - ![image](img/angular-init.png)

```
$ cd badges-angular-frontend

tvonstebut@PRI-HQ-023 MINGW64 ~/DEV/CNAM/NFA035/badges-frontend/badges-angular-frontend (nfa035/session4.4)
$ ng serve
- Generating browser application bundles (phase: setup)...
Compiling @angular/core : es2015 as esm2015
Compiling @angular/common : es2015 as esm2015
Compiling @angular/platform-browser : es2015 as esm2015
Compiling @angular/platform-browser-dynamic : es2015 as esm2015
√ Browser application bundle generation complete.

Initial Chunk Files   | Names         |      Size
vendor.js             | vendor        |   2.09 MB
polyfills.js          | polyfills     | 510.58 kB
styles.css, styles.js | styles        | 383.37 kB
main.js               | main          |  55.09 kB
runtime.js            | runtime       |   6.66 kB

| Initial Total |   3.02 MB

Build at: 2021-09-28T15:49:15.446Z - Hash: bf9bc9041ebb81799b3e - Time: 9181ms

** Angular Live Development Server is listening on localhost:4200, open your browser on http://localhost:4200/ **


√ Compiled successfully.
√ Browser application bundle generation complete.

5 unchanged chunks

Build at: 2021-09-28T15:49:16.211Z - Hash: 4a48c163a0d9a543494e - Time: 386ms

√ Compiled successfully.
```

### Ensuite, récupérer le générateur de code de Swagger et l'exécuter

**La denière version est préférable si l'on veut faire de l'Angular 10+.**

----

#### 1ère méthode: à l'aide de la librairie java de Swagger

Un peu de doc: https://swagger.io/docs/open-source-tools/swagger-codegen/ puis...

 * https://mvnrepository.com/artifact/io.swagger.codegen.v3/swagger-codegen-cli => 

`$ curl -O https://repo1.maven.org/maven2/io/swagger/codegen/v3/swagger-codegen-cli/3.0.27/swagger-codegen-cli-3.0.27.jar`

Génération du code Angular "backend" c'est-à-dire la partie module d'API, qui se déduit du contrat swagger aux normes OpenAPI v3

```
$ java -jar swagger-codegen-cli-3.0.27.jar generate -i http://localhost:8080/v3/api-docs  -l typescript-angular -o badges-angular-frontend/src/app/rest  --additional-properties=ngVersion=12.2.0,npmName=badgesFrontend,npmVersion=1.0.0
```

Cela sort quelque chose comme ceci en console normalement:

```
Version=12.2.0,npmName=badgesFrontend,npmVersion=1.0.0
17:20:44.976 [Thread-0] WARN  i.s.c.v.i.CodegenIgnoreProcessor - Output directory does not exist, or is inaccessible. No file (.swagger-codegen-ignore) will be evaluated.
WARNING: An illegal reflective access operation has occurred
...
WARNING: All illegal access operations will be denied in a future release
17:20:45.097 [Thread-0] INFO  i.s.codegen.v3.AbstractGenerator - writing file C:\Users\tvonstebut\DEV\CNAM\NFA035\badges-frontend\badges-angular-frontend\src\app\rest\model\badgesBody.ts
17:20:45.110 [Thread-0] INFO  i.s.codegen.v3.AbstractGenerator - writing file C:\Users\tvonstebut\DEV\CNAM\NFA035\badges-frontend\badges-angular-frontend\src\app\rest\model\digitalBadge.ts
....
17:20:45.434 [Thread-0] INFO  i.s.codegen.v3.AbstractGenerator - writing file C:\Users\tvonstebut\DEV\CNAM\NFA035\badges-frontend\badges-angular-frontend\src\app\rest\.swagger-codegen-ignore
17:20:45.435 [Thread-0] INFO  i.s.codegen.v3.AbstractGenerator - writing file C:\Users\tvonstebut\DEV\CNAM\NFA035\badges-frontend\badges-angular-frontend\src\app\rest\.swagger-codegen\VERSION

```

**Sauf que, avec une version trop récente du JDK, il y aura des problèmes (testé ok en version 13, KO en version 17, et certainement en version 16)**

:)

**Donc il est recommandé d'aller se documenter à la source, c'est à dire openAPI, c'est-à-dire l'original https://openapi-generator.tech/ qui, lui, est écrit en js (nodejs)**
Ceci étant dit, il promet bien plus de langages en génération, et forcément on se dégage des problèmes de fraîcheur liés au portage en java, puisqu'il s'agit bien de la source.

----

#### 2nde méthode, avec le module openapi-generator de nodejs

Depuis le module **badges-frontend**, toujours, exécuter:

`$ npm install @openapitools/openapi-generator-cli -g`

Puis ceci pour rétrograder un niveau de version et éviter, encore une fois un plantage (nodejs est un monde en constante ébullition :/ ):

`openapi-generator-cli version-manager set 5.1.0`

===> Au cas où le problème se corrige dans une version plus récente, sachez qu'au jour d'aujourd'hui, l'état des version est:

```
$ openapi-generator-cli version-manager list
? The following releases are available: (Use arrow keys)
  ☐  releasedAt  version      installed  versionTags
  -  ----------  -----------  ---------  ----------------------------
> ☐  2021-08-16  5.2.1        yes        5.2.1 stable latest
  ☐  2021-07-09  5.2.0        no         5.2.0 stable
  ☐  2021-05-07  5.1.1        no         5.1.1 stable
  ☒  2021-03-20  5.1.0        yes        5.1.0 stable
  ☐  2021-02-06  5.0.1        no         5.0.1 stable
(Move up and down to reveal more choices)
```

 * Bref, à présent vous pouvez générer avec le module node sous Java 16 ou 17:

```
openapi-generator-cli generate -i http://localhost:8080/v3/api-docs -g typescript-angular -o badges-angular-frontend/src/app/rest --additional-properties=ngVersion=12.2.0,withInterfaces=true,providedInRoot=true,npmName=badgesFrontend,npmVersion=1.0.0
```

Ce qui va vous sortir à peu près ceci en sortie console:

```
...
[main] INFO  o.o.codegen.TemplateManager - writing file C:\Users\tvonstebut\DEV\CNAM\NFA035\badges-frontend\badges-angular-frontend\src\app\rest\tsconfig.json
[main] INFO  o.o.codegen.TemplateManager - writing file C:\Users\tvonstebut\DEV\CNAM\NFA035\badges-frontend\badges-angular-frontend\src\app\rest\.openapi-generator-ignore
[main] INFO  o.o.codegen.TemplateManager - writing file C:\Users\tvonstebut\DEV\CNAM\NFA035\badges-frontend\badges-angular-frontend\src\app\rest\.openapi-generator\VERSION
[main] INFO  o.o.codegen.TemplateManager - writing file C:\Users\tvonstebut\DEV\CNAM\NFA035\badges-frontend\badges-angular-frontend\src\app\rest\.openapi-generator\FILES
################################################################################
# Thanks for using OpenAPI Generator.                                          #
# Please consider donation to help us maintain this project ?                 #
# https://opencollective.com/openapi_generator/donate                          #
################################################################################
```

**Voilà c'est bon**, si vous avez ce message sans ERREUR (les WARNINGS ok).

### Branchement du code généré sur notre application blanche initiale
 
Puis les opérations nécessaires pour effectuer les différents branchements nécessaires, par écriture d'un tout petit peu de code au passage...

```
# Cette première ligne est peut-être nécessaire si le module Angular n'est pas chargé
$ npm i rxjs-compat --save
```

##### Edition du code Typescript/HTML pour le brancher sur l'application initiale

On se retrouve avec un pattern MVC implémenté intrinsèquement à ce module. Eh oui, même s'il s'agit bien de la vue dans notre architecture principale, il y a encore un MVC au sein de cette vue.

#### Le modèle:  src/app/rest/model/*

La bonne nouvelle le modèle a été généré à la perfection, rien à refaire.

#### Le controller (composant dans Angular): src/app/app.component.ts

Pareillement, le controller est bon, au niveau src/app/rest/api , mais il faut le brancher au niveau du composant applicatif principal...

```java
import { Component } from '@angular/core';
import { BadgesWalletRestServiceImplService } from 'src/app/rest';
import { DigitalBadge } from 'src/app/rest/model/models';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'badges-angular-frontend';

  constructor(private service: BadgesWalletRestServiceImplService) { }

  ngOnInit() {
    this.service.getMetadata().subscribe(metas => {
      console.log(metas);
      this.badges = metas;
      this.myDataArray = [...metas];
    })
  }

  badges?: Set<DigitalBadge>;
  myDataArray: DigitalBadge[] = [];
  displayedColumns: string[] = ['badgeId', 'serial', 'begin', 'end', 'imageSize'];

}
```
#### La vue: src/app/app.component.html

Alléger la page html, voire tout enlever pour simplifier...puis insérer ce code:

```html
<h2>BadgesWallet</h2>
<p>Liste des Badges</p>
<table class="table table-striped table-bordered">
  <thead>
  <tr>
    <th>ID</th>
    <th>Serial</th>
    <th>Date début</th>
    <th>Date fin</th>
    <th>Taille de l'image</th>
  </tr>
  </thead>
  <tbody>
  <tr *ngFor="let badge of badges">
    <td>{{badge.metadata?.badgeId}}</td>
    <td>{{badge.serial}}</td>
    <td>{{badge.begin}}</td>
    <td>{{badge.end}}</td>
    <td>{{badge.metadata?.imageSize}}</td>
  </tr>
  </tbody>
</table>
```

#### Amélioration de la vue: app.component.html , version avancée avec angular-materials

 * Documentation: https://material.angular.io/components/table/overview
 * Ajout du module Angular-Material au projet Angular:
   * `$ ng add @angular/material --skip-confirmation`

```java
      <table mat-table [dataSource]="myDataArray" class="mat-elevation-z8">
        <ng-container matColumnDef="badgeId">
          <th mat-header-cell *matHeaderCellDef> ID </th>
          <td mat-cell *matCellDef="let badge"> {{badge.metadata?.badgeId}} </td>
        </ng-container>
        <ng-container matColumnDef="serial">
          <th mat-header-cell *matHeaderCellDef> Code de série </th>
          <td mat-cell *matCellDef="let badge"> {{badge.serial}} </td>
        </ng-container>
        <ng-container matColumnDef="begin">
          <th mat-header-cell *matHeaderCellDef> Date de Début </th>
          <td mat-cell *matCellDef="let badge"> {{badge.begin}} </td>
        </ng-container>
        <ng-container matColumnDef="end">
          <th mat-header-cell *matHeaderCellDef> Date de Fin </th>
          <td mat-cell *matCellDef="let badge"> {{badge.end}} </td>
        </ng-container>
        <ng-container matColumnDef="imageSize">
          <th mat-header-cell *matHeaderCellDef> Taille de l'image </th>
          <td mat-cell *matCellDef="let badge"> {{badge.metadata?.imageSize}} </td>
        </ng-container>

        <tr mat-header-row *matHeaderRowDef="displayedColumns"></tr>
        <tr mat-row *matRowDef="let row; columns: displayedColumns"></tr>
      </table>
  ```




## Demo time :

### Lancement de l'appli


```
$ ng build
$ ng serve
```

### Test local

 * Conseil pour y pervenir: s'aider de sortie dans les logs à droite sur cette image. 
 * Si cela ne compile pas vous pouvez 
   * mettre des lignes de code typeScript en commentaire (dans les fichiers .ts) et ajouter des lignes console.log avant, en échange
   * Mettre des balises html en commentaire également, car la compilation angular intègre aussi le html (car composants riches), pas que le typeScript.
 * Exemple:
  ```java
   ngOnInit() {
    this.service.getMetadata().subscribe(metas => {
      console.log(metas);
      //this.badges = metas;
      //this.myDataArray = [...metas];
    })
   }
  ```
  - * Dans l'exemple ci-dessus et en commentant également le tableau html, on peut déjà s'assurer que dans un premier temps, on arrive bien à loguer dans la console du navigateur, le contenu de notre Wallet (métadonnées).

![image](img/angular-debug.png)


----

C'est tout, voilà, et pour ceux qui s'y sont intéressé, j'espère que ce tuto BONUS vous a plu.
